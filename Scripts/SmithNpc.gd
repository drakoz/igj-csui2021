extends KinematicBody2D

export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var velocity = Vector2()
var player
var upgrade_interface
var can_interact = false

func _ready():
	$AnimationPlayer.play("idle")
	player = get_node("/root").get_child(1).get_node("PlayerKnight")
	upgrade_interface = get_node("/root").get_child(1).get_node("GUI").get_node("UpgradeInterface")
	upgrade_interface.visible = false

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	velocity = move_and_slide(velocity, UP)
	if player.get_global_position() >= self.get_global_position():
		$Sprite.scale.x = 1
	else:
		$Sprite.scale.x = -1
		
	if Input.is_action_just_pressed("interact") and can_interact:
		upgrade_interface.visible = !upgrade_interface.visible

func _on_InteractArea_body_entered(body):
	if body.name == "PlayerKnight":
		$Label.visible = true
		can_interact = true

func _on_InteractArea_body_exited(body):
	if body.name == "PlayerKnight":
		$Label.visible = false
		can_interact = false
		upgrade_interface.visible = false
