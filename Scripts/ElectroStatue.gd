extends Area2D

var upgrade_interface 
var gui

func _ready():
	if global.electro_buff:
		queue_free()

func _on_ElectroStatue_body_entered(body):
	if body.name == "PlayerKnight":
		global.electro_buff = true
		gui = get_node("/root").get_child(1).get_node("GUI")
		gui.get_node("ElectroBuff").visible = true
		gui.get_node("ElectroLabel").visible = true
		queue_free()
