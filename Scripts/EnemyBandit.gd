extends KinematicBody2D

const CoinDrop = preload("res://Scenes/Coin.tscn")
export (int) var speed = 100
export (int) var GRAVITY = 1200
export (int) var health = 100
export (float) var attack_delay = 1.5

const UP = Vector2(0,-1)

var state_machine
var velocity = Vector2()
var alive = true
var distance
var player
var player_detected = false
var detection_range = 400
var player_in_attack_area = false
var is_attacking = false
var can_attack = true
var time_passed = 0.0

func _ready():
	randomize()
	state_machine = $AnimationTree.get("parameters/playback")
	player = get_node("/root").get_child(1).get_node("PlayerKnight")
	
func _physics_process(delta):
	if alive:
		velocity.y += delta * GRAVITY
		velocity = move_and_slide(velocity, UP)
		if player_detected:
			chase()
		else:
			distance = self.get_global_position().distance_to(player.get_global_position())
			player_detected = true if distance < detection_range else false
		time_passed += delta
		if time_passed > attack_delay:
			can_attack = true
			
func chase():
	velocity.x = 0
	if not player.is_alive:
		state_machine.travel("idle")
		return
	if state_machine.get_current_node() == "attack" or not can_attack:
		return
	if player_in_attack_area or abs(player.get_global_position().x - self.get_global_position().x) < 10:
		state_machine.travel("attack")	
		can_attack = false
		time_passed = 0.0
		return
	if player.get_global_position() >= self.get_global_position():
		velocity.x += speed
		$Sprite.scale.x = -1
	else:
		velocity.x -= speed
		$Sprite.scale.x = 1
	if velocity.x != 0:
		state_machine.travel("run")

func enemy_hit(damage):
	health -= damage
	print("%s hit by %s damage, health left = %s" % [self.name, damage, health])
	if health <= 0:
		var drop
		alive = false
		GRAVITY = 0
		state_machine.travel("death")
		for _i in range(randi()%5+2):
			drop = CoinDrop.instance()
			drop.position = self.position
			get_node("/root").call_deferred("add_child", drop)
		get_node("CollisionShape2D").set_deferred("disabled", true)
	else:
		state_machine.travel("hurt")

func _on_DetectArea_body_entered(body):
	if body.name == "PlayerKnight":
		player_in_attack_area = true

func _on_DetectArea_body_exited(body):
	if body.name == "PlayerKnight":
		player_in_attack_area = false

func _on_HitArea_body_entered(body):
	if body.name == "PlayerKnight":
		body.hit(20)
