extends Control

var player
var sword
var sword_up
var shield
var shield_up
const sword_level = {
	1 : 0,
	2 : 10,
	3 : 20,
	4 : 30,
	5 : 40
}

const shield_level = {
	1 : 0,
	2 : 7.5,
	3 : 15,
	4 : 22.5,
	5 : 30
}

const cost = {
	2 : 25,
	3 : 50,
	4 : 75,
	5 : 100,
	6 : "-"
}

func _ready():
	player = get_node("/root").get_child(1).get_node("PlayerKnight")
	sword = $MarginContainer/HBoxContainer/VBoxContainer/SwordLabel
	sword_up = $MarginContainer/HBoxContainer/VBoxContainer/UpgradeSword
	shield = $MarginContainer/HBoxContainer/VBoxContainer2/ShieldLabel
	shield_up = $MarginContainer/HBoxContainer/VBoxContainer2/UpgradeShield
	refresh_text()

func _on_UpgradeSword_pressed():
	if global.sword_level != 5 and global.coins >= cost[global.sword_level+1]:
		print("ugraded")
		global.coins -= cost[global.sword_level+1]
		global.sword_level += 1
		global.bonus_damage = sword_level[global.sword_level]
		player.refresh_coin()
		refresh_text()
	else:
		print("not enough")

func _on_UpgradeShield_pressed():
	if  global.shield_level != 5 and global.coins >= cost[global.shield_level+1]:
		print("ugraded")
		global.coins -= cost[global.shield_level+1]
		global.shield_level += 1
		global.bonus_block = shield_level[global.shield_level] + (10 if global.geo_buff else 0)
		player.refresh_coin()
		refresh_text()
	else:
		print("not enough")

func refresh_text():
	sword.text = "Lv. %s Sword (%s Damage)" % [global.sword_level, global.bonus_damage + player.base_damage]
	sword_up.text = "Upgrade (%s Gold)" % cost[global.sword_level+1]
	shield.text = "Lv. %s Shield (%s%% Reduction)" % [global.shield_level, global.bonus_block + player.base_blocked_percentage]
	shield_up.text = "Upgrade (%s Gold)" % cost[global.shield_level+1]
