extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -700
export (int) var GRAVITY = 1200

const UP = Vector2(0,-1)

var current_speed = speed
var velocity = Vector2()
var jump = false
var state_machine
var attack_index = 1
var attack_delay = 0.1
var combo_end = 0.5
var buff_duration = 15.0
var buff_cooldown = 30.0
var next_buff = 0.0
var can_attack = true
var can_buff = true
var on_buff = false
var time_passed = 0.0

func _ready():
	$AnimationPlayer.play("idle")
	state_machine = $AnimationTree.get("parameters/playback")
	
func _process(delta):
	time_passed += delta
	next_buff -= delta
	if time_passed > combo_end:
		can_attack = true
		attack_index = 1
	if next_buff < buff_duration:
		on_buff = false
	if next_buff < 0:
		can_buff = true

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
	
func attack():
	if can_attack:
		state_machine.travel("attack%s" % attack_index)
		attack_index += 1
		if attack_index == 5:
			attack_index = 1
			can_attack = false
		time_passed = 0
		
func buff():
	if can_buff:
		state_machine.travel("buff")
		can_buff = false
		on_buff = true
		next_buff = buff_cooldown
	
func get_input():
	velocity.x = 0
	if on_buff:
		current_speed = speed * 2
	else:
		current_speed = speed
	if Input.is_action_just_pressed("attack"):
		attack()
		return
	if Input.is_action_just_pressed("buff"):
		buff()
		return
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
		jump = true
	elif jump and Input.is_action_just_pressed('up'):
		# simple double jump 
		velocity.y = jump_speed * 2/3
		jump = false
	if Input.is_action_pressed('right'):
		velocity.x += current_speed
	if Input.is_action_pressed('left'):
		velocity.x -= current_speed
	if velocity.x == 0:
		state_machine.travel("idle")
	if velocity.x != 0:
		state_machine.travel("walk")
		if velocity.x > 0:
			$Sprite.scale.x = 1
		else:
			$Sprite.scale.x = -1
	if Input.is_action_just_pressed('reset'):
		get_tree().reload_current_scene()
