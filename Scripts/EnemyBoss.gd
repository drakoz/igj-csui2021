extends KinematicBody2D

const CoinDrop = preload("res://Scenes/Coin.tscn")
const ElectroDrop = preload("res://Scenes/ElectroStatue.tscn")
export (int) var speed = 100
export (int) var GRAVITY = 1200
export (int) var health = 1000
export (float) var attack_delay = 3

const UP = Vector2(0,-1)

var current_health = health + 0.0
var velocity = Vector2()
var alive = true
var distance
var player
var player_detected = false
var detection_range = 400
var anim
var can_attack = true
var player_in_attack_area = false
var time_passed = 0.0
var phase2 = false
var gui
var health_bar

func _ready():
	randomize()
	anim = $AnimatedSprite
	anim.play("idle")
	player = get_node("/root").get_child(1).get_node("PlayerKnight")
	gui = get_node("/root").get_child(1).get_node("GUI")
	health_bar = gui.get_node("BossHealth")
	health_bar.visible = true
	refresh_healthbar()
	$AnimatedSprite/UltiArea/UltiDetect.disabled = true

func _physics_process(delta):
	if alive:
		velocity.y += delta * GRAVITY
		velocity = move_and_slide(velocity, UP)
		if player_detected:
			chase()
		else:
			distance = self.get_global_position().distance_to(player.get_global_position())
			player_detected = true if distance < detection_range else false
			
func chase():
	velocity.x = 0
	if not player.is_alive:
		anim.play("idle")
		anim.set_offset(Vector2(0,0))
		return
	if anim.get_animation() == "taunt":
		current_health = 500.0
		refresh_healthbar()
		$AnimatedSprite/HitArea/HitArea1.set_deferred("disabled",true)
		$AnimatedSprite/HitArea/HitArea2.set_deferred("disabled",true)
		if anim.frame == anim.frames.get_frame_count("taunt")-1:
			anim.play("walk")
			$AnimatedSprite/UltiArea/UltiDetect.disabled = false
		return
	if anim.get_animation() == "attack":
		handle_attack_hitbox()
		if anim.frame == (anim.frames.get_frame_count("attack") if not phase2 else 20)-1:
			anim.play("walk")
		return
	if anim.get_animation() == "ulti":
		handle_ulti_hitbox()
		if anim.frame == anim.frames.get_frame_count("ulti")-1:
			anim.set_offset(Vector2(0,0))
			self.position += Vector2(-160, 0) * (-1 if anim.scale.x < 0 else 1)
			anim.play("walk")
		return
	if player_in_attack_area or abs(player.get_global_position().x - self.get_global_position().x) < 10:
		anim.play("attack")	
		time_passed = 0.0
		return
	if player.get_global_position() >= self.get_global_position():
		velocity.x += speed
		anim.scale.x = -1.5
	else:
		velocity.x -= speed
		anim.scale.x = 1.5
	if velocity.x != 0:
		anim.play("walk")
	
func enemy_hit(damage):
	current_health -= damage
	print("%s hit by %s damage, health left = %s" % [self.name, damage, current_health])
	if current_health <= 200 and not phase2:
		anim.play("taunt")
		speed *= 2
		phase2 = true
		$AnimatedSprite/HitArea/HitArea1.set_deferred("disabled",true)
		$AnimatedSprite/HitArea/HitArea2.set_deferred("disabled",true)
	if current_health <= 0:
		health_bar.visible = false
		var drop
		alive = false
		GRAVITY = 0
		anim.play("death")
		anim.set_offset(Vector2(0,0))
		if not global.electro_buff:
			drop = ElectroDrop.instance()
			drop.position = self.position + Vector2(0, -20)
			get_node("/root").call_deferred("add_child", drop)
		$AnimatedSprite/UltiArea/UltiDetect.set_deferred("disabled",true)
		$AnimatedSprite/HitArea/HitArea1.set_deferred("disabled",true)
		$AnimatedSprite/HitArea/HitArea2.set_deferred("disabled",true)
		for _i in range(randi()%100+50):
			drop = CoinDrop.instance()
			drop.position = self.position
			get_node("/root").call_deferred("add_child", drop)
		get_node("/root").get_child(1).get_node("Gate").visible = true
		get_node("CollisionShape2D").set_deferred("disabled", true)
	refresh_healthbar()
		
func handle_attack_hitbox():
	if anim.frame == 5:
		$AnimatedSprite/HitArea/HitArea1.disabled = false
	if anim.frame == 8:
		$AnimatedSprite/HitArea/HitArea1.disabled = true
	if anim.frame == 14:
		$AnimatedSprite/HitArea/HitArea2.disabled = false
	if anim.frame == 17:
		$AnimatedSprite/HitArea/HitArea2.disabled = true
	if anim.frame == 23:
		$AnimatedSprite/HitArea/HitArea2.disabled = false
	if anim.frame == 26:
		$AnimatedSprite/HitArea/HitArea2.disabled = true
		
func handle_ulti_hitbox():
	if anim.frame == 19:
		$AnimatedSprite/UltiHitArea/UltiHit.disabled = false
	if anim.frame == 23:
		$AnimatedSprite/UltiHitArea/UltiHit.disabled = true
	if anim.frame == 26:
		$AnimatedSprite/UltiHitArea/UltiHit.disabled = false
	if anim.frame == 30:
		$AnimatedSprite/UltiHitArea/UltiHit.disabled = true

func _on_DetectArea_body_entered(body):
	if body.name == "PlayerKnight":
		player_in_attack_area = true

func _on_DetectArea_body_exited(body):
	if body.name == "PlayerKnight":
		player_in_attack_area = false
		
func _on_UltiArea_body_entered(body):
	if body.name == "PlayerKnight" and alive:
		if anim.get_animation() != "attack":
			anim.play("ulti")
			anim.set_offset(Vector2(-50,-15))
			
func _on_HitArea_body_entered(body):
	if body.name == "PlayerKnight":
		body.hit(25)
		
func _on_UltiHitArea_body_entered(body):
	if body.name == "PlayerKnight":
		body.hit(40)

func refresh_healthbar():
	health_bar.get_child(0).value = current_health/health * 100
	health_bar.get_child(0).get_child(0).text = "%s/%s" % [current_health if alive else 0,health]
