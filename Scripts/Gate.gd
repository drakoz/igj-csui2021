extends Node2D

var player
var select_level_interface
var can_interact = false
var is_village

func _ready():
	player = get_node("/root").get_child(1).get_node("PlayerKnight")
	select_level_interface = get_node("/root").get_child(1).get_node("GUI").get_node("LevelSelectInterface")
	select_level_interface.visible = false
	is_village = true if get_node("/root").get_child(1).name == "Village" else false
	$Label.text = "Enter (E)" if is_village else "Return (E)"

func _process(_delta):
	if player.get_global_position() >= self.get_global_position():
		$Sprite.scale.x = 1
	else:
		$Sprite.scale.x = -1
		
	if Input.is_action_just_pressed("interact") and can_interact:
		select_level_interface.visible = !select_level_interface.visible
		
	if Input.is_action_just_pressed("interact") and can_interact and not is_village:
		global.coins += global.coins_earned_this_floor
		global.coins_earned_this_floor = 0
		get_tree().change_scene(str("res://Scenes/Village.tscn"))

func _on_InteractArea_body_entered(body):
	if body.name == "PlayerKnight":
		$Label.visible = true
		can_interact = true

func _on_InteractArea_body_exited(body):
	if body.name == "PlayerKnight":
		$Label.visible = false
		can_interact = false
		select_level_interface.visible = false
