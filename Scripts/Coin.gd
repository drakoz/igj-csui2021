extends Area2D

var velocity = Vector2()
var on_ground = false
var random_x

func _ready():
	randomize()
	random_x = rand_range(-100,100)
	
func _process(delta):
	if not on_ground:
		velocity.x += random_x * 5 * delta
		velocity.y += gravity * 2 * delta 
		position += velocity * delta

func _on_Coin_body_entered(body):
	if body.has_method("get_coin"):
		body.get_coin()
		queue_free()
	if body.name == "Ground":
		on_ground = true
