extends Area2D

var upgrade_interface 
var gui

func _ready():
	if global.geo_buff:
		queue_free()

func _on_GeoStatue_body_entered(body):
	if body.name == "PlayerKnight":
		global.geo_buff = true
		global.bonus_block += 10
		gui = get_node("/root").get_child(1).get_node("GUI")
		upgrade_interface = gui.get_node("UpgradeInterface")
		upgrade_interface.refresh_text()
		gui.get_node("GeoBuff").visible = true
		gui.get_node("GeoLabel").visible = true
		queue_free()
