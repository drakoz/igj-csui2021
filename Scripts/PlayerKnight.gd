extends KinematicBody2D

export (int) var speed = 400
export (int) var jump_speed = -700
export (int) var GRAVITY = 1200
export (int) var health = 150
export (int) var base_damage = 20
export (int) var base_blocked_percentage = 50

const UP = Vector2(0,-1)

var current_health
var gui
var health_bar
var coin_count
var current_speed = speed
var velocity = Vector2()
var is_jumping = false
var state_machine
var attack_index = 1
var attack_delay = 0.4
var combo_end = 0.7
var buff_duration = 15.0
var buff_cooldown = 30.0
var next_buff = 0.0
var can_attack = true
var can_buff = true
var on_buff = false
var time_passed = 0.0
var is_alive = true
var is_hurt = false
var is_blocking = false

func _ready():
	current_health = health + 0.0
	state_machine = $AnimationTree.get("parameters/playback")
	gui = get_node("/root").get_child(1).get_node("GUI")
	health_bar = gui.get_node("HealthBar").get_child(0)
	coin_count = gui.get_node("CoinCount").get_node("Label")
	gui.get_node("DeathLabel").visible = false
	if global.geo_buff:
		gui.get_node("GeoBuff").visible = true
		gui.get_node("GeoLabel").visible = true
	if global.electro_buff:
		gui.get_node("ElectroBuff").visible = true
		gui.get_node("ElectroLabel").visible = true
	refresh_healthbar()
	refresh_coin()
	
func _process(delta):
	time_passed += delta
	next_buff -= delta
	if time_passed > attack_delay and attack_index != 4:
		can_attack = true
	if time_passed > combo_end + 0.2:
		can_attack = true
		attack_index = 1
	if next_buff < buff_duration:
		on_buff = false
	if next_buff < 0:
		can_buff = true
	if state_machine.get_current_node() != "hurt":
		is_hurt = false

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	if is_alive:
		get_input()
	else:
		velocity.x = 0
	velocity = move_and_slide(velocity, UP)
	if Input.is_action_just_pressed('reset') and not is_alive:
		get_tree().change_scene(str("res://Scenes/Village.tscn"))
		global.coins_earned_this_floor = 0
		
func attack():
	if can_attack:
		state_machine.travel("attack%s" % attack_index)
		attack_index += 1
		can_attack = false
		time_passed = 0
		
func buff():
	can_buff = false
	on_buff = true
	next_buff = buff_cooldown
		
func is_attacking():
	if attack_index != 4:
		return time_passed < attack_delay
	else:
		return time_passed < combo_end
	
func get_input():
	velocity.x = 0
	if on_buff:
		current_speed = speed * 2
	else:
		current_speed = speed
	if is_on_floor() and Input.is_action_pressed("block"):
		is_blocking = true
		if is_hurt:
			state_machine.travel("blocked")
		else:
			state_machine.travel("blocking")
		return
	else:
		is_blocking = false
	if is_on_floor() and velocity.x == 0 and Input.is_action_just_pressed("attack"):
		attack()
		return
	if Input.is_action_just_pressed("buff") and can_buff and global.electro_buff:
		buff()
		return
	if is_on_floor() and Input.is_action_just_pressed('up') and not is_attacking():
		velocity.y = jump_speed
		is_jumping = true
	elif is_jumping and Input.is_action_just_pressed('up') and not is_attacking():
		# simple double jump 
		velocity.y = jump_speed * 2/3
		is_jumping = false
	if Input.is_action_pressed('right') and not is_attacking():
		velocity.x += current_speed
	if Input.is_action_pressed('left') and not is_attacking():
		velocity.x -= current_speed
	if velocity.x == 0 and not is_hurt:
		state_machine.travel("idle")
	if velocity.x != 0:
		state_machine.travel("run")
		if velocity.x > 0:
			$Sprite.scale.x = 1
		else:
			$Sprite.scale.x = -1
	if velocity.y < 0 and not is_on_floor():
		state_machine.travel("jump")
	if velocity.y > 0 and not is_on_floor():
		state_machine.travel("fall")

func _on_HitArea_body_entered(body):
	if body.has_method("enemy_hit") and body.alive:
		body.enemy_hit(deal_damage())

func deal_damage():
	if attack_index == 4:
		return (base_damage + global.bonus_damage) * 1.5
	else:
		return base_damage + global.bonus_damage

func get_coin():
	global.coins_earned_this_floor += 1
	refresh_coin()
	
func hit(damage_received):
	if is_blocking:
		damage_received -= damage_received * (base_blocked_percentage + global.bonus_block)/100
	current_health -= damage_received
	if current_health <= 0:
		is_alive = false
		state_machine.travel("death")
		gui.get_node("DeathLabel").visible = true
	else:
		if not is_blocking:
			state_machine.travel("hurt")
		is_hurt = true
	refresh_healthbar()

func refresh_healthbar():
	health_bar.value = current_health/health * 100
	health_bar.get_child(0).text = "%s/%s" % [current_health if is_alive else 0,health]
	
func refresh_coin():
	coin_count.text = "%s" % (global.coins + global.coins_earned_this_floor)
